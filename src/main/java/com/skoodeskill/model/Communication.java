package com.skoodeskill.model;

/**
 * Created by PeshZ on 9/28/15.
 */
public class Communication {
    private long tmpId;
    private long id;
    private String name;
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getTmpId() {
        return tmpId;
    }

    public void setTmpId(long tmpId) {
        this.tmpId = tmpId;
    }

    @Override
    public String toString() {
        return "Communication{" +
                "tmpId=" + tmpId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
