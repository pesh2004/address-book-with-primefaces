package com.skoodeskill.model;

import java.util.Date;
import java.util.List;

/**
 * Created by PeshZ on 9/24/15.
 */
public class Employee {
    private Long id;
    private String title;
    private String firstName;
    private String lastName;
    private String gender;
    private Date birthDay;
    private String flag;
    private String homeAddressNo;
    private String homeRoad;
    private String homeProvince;
    private String homeDistrinct;
    private int homePostcode;
    private String workAddressNo;
    private String workRoad;
    private String workProvince;
    private String workDistrinct;
    private int workPostcode;
    private List<Communication> communicationList;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHomeAddressNo() {
        return homeAddressNo;
    }

    public void setHomeAddressNo(String homeAddressNo) {
        this.homeAddressNo = homeAddressNo;
    }

    public String getHomeRoad() {
        return homeRoad;
    }

    public void setHomeRoad(String homeRoad) {
        this.homeRoad = homeRoad;
    }

    public String getHomeProvince() {
        return homeProvince;
    }

    public void setHomeProvince(String homeProvince) {
        this.homeProvince = homeProvince;
    }

    public String getHomeDistrinct() {
        return homeDistrinct;
    }

    public void setHomeDistrinct(String homeDistrinct) {
        this.homeDistrinct = homeDistrinct;
    }

    public int getHomePostcode() {
        return homePostcode;
    }

    public void setHomePostcode(int homePostcode) {
        this.homePostcode = homePostcode;
    }

    public String getWorkRoad() {
        return workRoad;
    }

    public void setWorkRoad(String workRoad) {
        this.workRoad = workRoad;
    }

    public String getWorkProvince() {
        return workProvince;
    }

    public void setWorkProvince(String workProvince) {
        this.workProvince = workProvince;
    }

    public String getWorkDistrinct() {
        return workDistrinct;
    }

    public void setWorkDistrinct(String workDistrinct) {
        this.workDistrinct = workDistrinct;
    }

    public int getWorkPostcode() {
        return workPostcode;
    }

    public void setWorkPostcode(int workPostcode) {
        this.workPostcode = workPostcode;
    }

    public String getWorkAddressNo() {
        return workAddressNo;
    }

    public void setWorkAddressNo(String workAddressNo) {
        this.workAddressNo = workAddressNo;
    }

    public List<Communication> getCommunicationList() {
        return communicationList;
    }

    public void setCommunicationList(List<Communication> communicationList) {
        this.communicationList = communicationList;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", birthDay=" + birthDay +
                ", flag='" + flag + '\'' +
                ", homeAddressNo='" + homeAddressNo + '\'' +
                ", homeRoad='" + homeRoad + '\'' +
                ", homeProvince='" + homeProvince + '\'' +
                ", homeDistrinct='" + homeDistrinct + '\'' +
                ", homePostcode='" + homePostcode + '\'' +
                ", workAddressNo='" + workAddressNo + '\'' +
                ", workRoad='" + workRoad + '\'' +
                ", workProvince='" + workProvince + '\'' +
                ", workDistrinct='" + workDistrinct + '\'' +
                ", workPostcode='" + workPostcode + '\'' +
                ", communicationList=" + communicationList +
                '}';
    }
}
