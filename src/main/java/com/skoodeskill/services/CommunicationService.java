package com.skoodeskill.services;

import com.skoodeskill.mappers.CommunicationMapper;
import com.skoodeskill.model.Communication;
import org.apache.ibatis.session.SqlSession;
import org.primefaces.context.RequestContext;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PeshZ on 9/28/15.
 */
@ManagedBean(name = "communication")
@SessionScoped
public class CommunicationService implements CommunicationMapper {


    public List<Communication> communicationList = new ArrayList<Communication>();
    private Communication communicationEntity;
    private int selectCommunication;

    @Override
    public List<Communication> getAllCommunication() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            return communicationMapper.getAllCommunication();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Communication getCommunicationById(Long communicationId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            return communicationMapper.getCommunicationById(communicationId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Communication> getCommunicationByIdList(Long communicationId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            return communicationMapper.getCommunicationByIdList(communicationId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int insertCommunication(Communication communication) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            int effectRow = communicationMapper.insertCommunication(communication);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int updateCommunication(Communication communication) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            int effectRow = communicationMapper.updateCommunication(communication);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int deleteCommunication(Long communicationId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            int effectRow = communicationMapper.deleteCommunication(communicationId);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Communication> findCommunicationByValue(Long employeeId, String searchValue) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            CommunicationMapper communicationMapper = sqlSession.getMapper(CommunicationMapper.class);
            return communicationMapper.findCommunicationByValue(employeeId,searchValue);
        } finally {
            sqlSession.close();
        }
    }

    public void addNewCommunicationRow(String type,String value){
        Communication communication = new Communication();
        int id = communicationList.size() + 1;
        communication.setId(id);
        communication.setName(type);
        communication.setValue(value);
        communicationList.add(communication);
    }

    public Communication getCommunicationListById(int rowId){
        rowId = rowId-1;
        return communicationList.get(rowId);
    }

    public List<Communication> getCommunicationList(){
        return this.communicationList;
    }

    public void editTempCommunication(int id){
        communicationEntity = getCommunicationListById(id);
        selectCommunication = (int) communicationEntity.getId();
    }

    public void setCommunicationEntity(Communication communication){
        this.communicationEntity = communication;
    }

    public Communication getCommunicationEntity(){
        return this.communicationEntity;
    }

    public void addTempCommunication(){
        selectCommunication = 0;
        communicationEntity = new Communication();
    }

    public void saveTmpData(){
        boolean success;
        long tmpId = communicationEntity.getTmpId();
        Communication communication = getCommunicationById(Long.valueOf(selectCommunication));
        if(tmpId == 0){
            success = true;
            int id = communicationList.size() + 1;
            communicationEntity.setTmpId(id);
            communicationEntity.setId(communication.getId());
            communicationEntity.setName(communication.getName());
            communicationEntity.setValue(communicationEntity.getValue());
            communicationList.add(communicationEntity);
        }else {
            success = true;
            communicationEntity = getCommunicationListById((int) tmpId);
            communicationEntity.setTmpId(tmpId);
            communicationEntity.setId(communication.getId());
            communicationEntity.setName(communication.getName());
            communicationEntity.setValue(communicationEntity.getValue());
            int index = (int) (tmpId - 1);
            communicationList.set(index,communicationEntity);
        }
        if (success) {
            RequestContext.getCurrentInstance().execute("PF('communicationDia').hide()");
        }
    }

    public void deleteTmpData(int communicationTempId){
        System.out.println(communicationList.size());
        int index = communicationTempId - 1;
        communicationList.remove(index);
        System.out.println(communicationList.size());
    }

    public void clearCommunicationList(){
        communicationList = new ArrayList<Communication>();
    }


    public int getSelectCommunication() {
        return selectCommunication;
    }

    public void setSelectCommunication(int selectCommunication) {
        this.selectCommunication = selectCommunication;
    }

}
