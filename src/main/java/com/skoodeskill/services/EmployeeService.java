package com.skoodeskill.services;

import com.skoodeskill.mappers.EmployeeMapper;
import com.skoodeskill.model.Communication;
import com.skoodeskill.model.Employee;
import org.apache.ibatis.session.SqlSession;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PeshZ on 9/24/15.
 */
@ManagedBean(name = "employee")
@SessionScoped
public class EmployeeService implements EmployeeMapper {

    private Employee employeeEntity;
    private List<Employee> employeeList;
    CommunicationService communicationService = getCommunicationService();
    private String summary;
    private String summaryMessage;
    private String employeeId;
    private String searchResult;

    public CommunicationService getCommunicationService() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return  (CommunicationService) facesContext.getApplication().getVariableResolver().resolveVariable(facesContext, "communication");
    }

    public void setCommunicationService(CommunicationService communicationService) {
        this.communicationService = communicationService;
    }

    @Override
    public List<Employee> getAllEmployee() {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getAllEmployee();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public Employee getEmployeeById(Long employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.getEmployeeById(employeeId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int insertEmployee(Employee employee) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.insertEmployee(employee);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int insertCommunicationValue(Long employeeId, Long communicationId, String value) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.insertCommunicationValue(employeeId, communicationId, value);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int updateEmployee(Employee employee) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.updateEmployee(employee);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int deleteEmployee(Long employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.deleteEmployee(employeeId);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int deleteCommunicationValue(Long employeeId) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            int effectRow = employeeMapper.deleteCommunicationValue(employeeId);
            sqlSession.commit();
            return effectRow;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<Employee> findEmployeeByValue(String value) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try {
            EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
            return employeeMapper.findEmployeeByValue(value);
        } finally {
            sqlSession.close();
        }
    }


    public String createForm() {
        communicationService.clearCommunicationList();
        return "/Employee/form.xhtml?faces-redirect=true";
    }

    public String editForm(int id){
        communicationService.clearCommunicationList();
        return "/Employee/form.xhtml?faces-redirect=true&employeeId="+id;
    }

    public void startForm(){
        FacesContext context = FacesContext.getCurrentInstance();
        employeeId = context.getExternalContext().getRequestParameterMap().get("employeeId");
        if (employeeId == null){
            employeeEntity = new Employee();
        }else {
            employeeEntity = getEmployeeById(Long.valueOf(employeeId));
            long i = 0;
            for (Communication communication1 :employeeEntity.getCommunicationList()){
                i++;
                Communication communication = new Communication();
                communication.setTmpId(i);
                communication.setId(communication1.getId());
                communication.setName(communication1.getName());
                communication.setValue(communication1.getValue());
                communicationService.communicationList.add(communication);
            }
        }
    }


    public Employee getEmployeeEntity() {
        return employeeEntity;
    }

    public void setEmployeeEntity(Employee employeeEntity) {
        this.employeeEntity = employeeEntity;
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }


    public void saveEmployeeData(){
        String redirect = "";
        List<Communication> communicationList = communicationService.getCommunicationList();
        if(communicationList.size() > 0){
            try {
                employeeId = String.valueOf(employeeEntity.getId());
                System.out.println(employeeId);
                if (Integer.parseInt(employeeId) == 0){
                    System.out.println("insert");
                    int effect = insertEmployee(employeeEntity);
                    summaryMessage = "เพิ่มข้อมูลสำเร็จ";
                }else {
                    System.out.println("update");
                    int effect = updateEmployee(employeeEntity);
                    int effectDelete = deleteCommunicationValue(Long.valueOf(employeeId));
                    summaryMessage = "แก้ไขข้อมูลสำเร็จ";
                }
                for (Communication communication:communicationList){
                    int effectRow = insertCommunicationValue(employeeEntity.getId(), communication.getId(), communication.getValue());
                }
                summary = "Success";
                redirect = "/Employee/index.xhtml";
                FacesContext.getCurrentInstance().getExternalContext().redirect(redirect);
            }catch (Exception e){
                System.out.println(e);
                summary = "Error";
                summaryMessage = "มีข้อผิดพลาด กรุณากรอกข้อมูลให้ครบ";
                showMessage();
            }
        }else {
            System.out.println(employeeEntity);
            summary = "Error";
            summaryMessage = "กรุณาเพิ่มข้อมูลการติดต่อด้วยครับ";
            FacesContext context = FacesContext.getCurrentInstance();
            System.out.println(context.isValidationFailed());
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, summaryMessage));
        }
    }

    public void showMessage(){
        if (summary != null && summaryMessage != null){
            addMessage(summary,summaryMessage);
        }
    }

    public void addMessage(String summary, String summaryMessage) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, summary,summaryMessage));
    }

    public String getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(String searchResult) {
        this.searchResult = searchResult;
    }

    public List<String> actionSearch(String query){
        List<String> results = new ArrayList<String>();
        List<Employee> employeeByValue = findEmployeeByValue(query);
        for (Employee employee:employeeByValue){
            results.add(employee.getFirstName() + " " + employee.getLastName());
        }
        System.out.println(results);
        return results;
    }
}
