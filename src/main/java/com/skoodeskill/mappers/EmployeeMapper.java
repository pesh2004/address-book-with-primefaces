package com.skoodeskill.mappers;

import com.skoodeskill.model.Employee;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by PeshZ on 9/24/15.
 */
public interface EmployeeMapper {

    @Select("select * from employee")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "birthDay", column = "birth_day"),
            @Result(property = "flag", column = "flag"),
            @Result(property = "homeAddressNo", column = "home_address_no"),
            @Result(property = "homeRoad", column = "home_road"),
            @Result(property = "homeProvince", column = "home_province"),
            @Result(property = "homeDistrinct", column = "home_distrinct"),
            @Result(property = "homePostcode", column = "home_postcode"),
            @Result(property = "workAddressNo", column = "work_address_no"),
            @Result(property = "workRoad", column = "work_road"),
            @Result(property = "workProvince", column = "work_province"),
            @Result(property = "workDistrinct", column = "work_distrinct"),
            @Result(property = "workPostcode", column = "work_postcode"),
            @Result(property = "communicationList", column = "id",many = @Many(select = "com.skoodeskill.mappers.CommunicationMapper.getCommunicationByIdList"))
    })
    public List<Employee> getAllEmployee();

    @Select("select * from employee as e where e.id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "birthDay", column = "birth_day"),
            @Result(property = "flag", column = "flag"),
            @Result(property = "homeAddressNo", column = "home_address_no"),
            @Result(property = "homeRoad", column = "home_road"),
            @Result(property = "homeProvince", column = "home_province"),
            @Result(property = "homeDistrinct", column = "home_distrinct"),
            @Result(property = "homePostcode", column = "home_postcode"),
            @Result(property = "workAddressNo", column = "work_address_no"),
            @Result(property = "workRoad", column = "work_road"),
            @Result(property = "workProvince", column = "work_province"),
            @Result(property = "workDistrinct", column = "work_distrinct"),
            @Result(property = "workPostcode", column = "work_postcode"),
            @Result(property = "communicationList", column = "id",many = @Many(select = "com.skoodeskill.mappers.CommunicationMapper.getCommunicationByIdList"))
    })
    public Employee getEmployeeById(Long employeeId);


    @Insert("INSERT INTO employee (title,first_name,last_name,gender,birth_day,flag,home_address_no,home_road,home_province,home_distrinct,home_postcode,work_address_no,work_road,work_province,work_distrinct,work_postcode) " +
                "VALUES (#{title},#{firstName},#{lastName},#{gender},#{birthDay},#{flag},#{homeAddressNo},#{homeRoad},#{homeProvince},#{homeDistrinct},#{homePostcode},#{workAddressNo},#{workRoad},#{workProvince},#{workDistrinct},#{workPostcode})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertEmployee(Employee employee);

    @Insert("insert into employee_communication(employee_id,communication_id,value)values(#{param1},#{param2},#{param3})")
    public int insertCommunicationValue(Long employeeId,Long communicationId,String value);

    @Update("update employee set title = #{title}, first_name = #{firstName}, last_name = #{lastName}, gender = #{gender}, birth_day = #{birthDay}, flag = #{flag}, home_address_no = #{homeAddressNo}, home_road = #{homeRoad}, home_province = #{homeProvince}, home_distrinct = #{homeDistrinct}, home_postcode = #{homePostcode},work_address_no = #{workAddressNo}, work_road = #{workRoad}, work_province = #{workProvince}, work_distrinct = #{workDistrinct}, work_postcode = #{workPostcode} where id = #{id}")
    public int updateEmployee(Employee employee);

    @Delete("delete from employee where id = #{id}")
    public int deleteEmployee(Long employeeId);

    @Delete("delete from employee_communication where employee_id = #{id}")
    public int deleteCommunicationValue(Long employeeId);


    @Select("select * from employee as e where LOWER(e.first_name) like LOWER(CONCAT('%', #{param1}, '%')) or LOWER(e.last_name) like LOWER(CONCAT('%', #{param1}, '%'))")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "gender", column = "gender"),
            @Result(property = "birthDay", column = "birth_day"),
            @Result(property = "flag", column = "flag"),
            @Result(property = "homeAddressNo", column = "home_address_no"),
            @Result(property = "homeRoad", column = "home_road"),
            @Result(property = "homeProvince", column = "home_province"),
            @Result(property = "homeDistrinct", column = "home_distrinct"),
            @Result(property = "homePostcode", column = "home_postcode"),
            @Result(property = "workAddressNo", column = "work_address_no"),
            @Result(property = "workRoad", column = "work_road"),
            @Result(property = "workProvince", column = "work_province"),
            @Result(property = "workDistrinct", column = "work_distrinct"),
            @Result(property = "workPostcode", column = "work_postcode"),
            @Result(property = "communicationList", column = "{id=employee_id}",many = @Many(select = "com.skoodeskill.mappers.CommunicationMapper.findCommunicationByValue"))
    })
    public List<Employee> findEmployeeByValue(String val);

}
