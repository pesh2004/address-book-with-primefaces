package com.skoodeskill.mappers;

import com.skoodeskill.model.Communication;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * Created by PeshZ on 9/28/15.
 */
public interface CommunicationMapper {
    @Select("select * from Communication")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name")
    })
    public List<Communication> getAllCommunication();

    @Select("select * from Communication as c where c.id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name")
    })
    public Communication getCommunicationById(Long communicationId);

    @Select("select * from Communication as c inner join employee_communication as ec on ec.communication_id = c.id where ec.employee_id = #{id}")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "value", column = "value")
    })
    public List<Communication> getCommunicationByIdList(Long employeeId);

    @Insert("insert into communication(name)values(#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    public int insertCommunication(Communication communication);

    @Update("update communication set name = #{name} where id = #{id}")
    public int updateCommunication(Communication communication);

    @Delete("delete from communication where id = #{id}")
    public int deleteCommunication(Long communicationId);

    @Select("select * from Communication as c inner join employee_communication as ec on ec.communication_id = c.id where ec.employee_id = #{id} and LOWER(ec.value) LIKE LOWER(CONCAT('%', #{param2}, '%'))")
    @Results({
            @Result(id=true, property = "id",column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "value", column = "value")
    })
    public List<Communication> findCommunicationByValue(Long employeeId,String searchValue);
}
