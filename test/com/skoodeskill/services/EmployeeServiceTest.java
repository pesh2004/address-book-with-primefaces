package com.skoodeskill.services;

import com.skoodeskill.model.Employee;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by PeshZ on 9/25/15.
 */
public class EmployeeServiceTest {

    private EmployeeService employeeService;

    @Before
    public void setUp() throws Exception {
        employeeService = new EmployeeService();
    }

    @After
    public void tearDown() throws Exception {
        employeeService = null;
    }

    @Test
    public void testGetAllEmployee() throws Exception {
        Assert.assertNotNull(employeeService);
        List<Employee> employees = employeeService.getAllEmployee();
        Assert.assertNotNull(employees);
        for (Employee employee:employees){
            System.out.println(employee.toString());
        }
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        Assert.assertNotNull(employeeService);
        Employee employee = new Employee();
        employee.setId(1L);
        Employee employeeById = employeeService.getEmployeeById(employee.getId());
        Assert.assertNotNull(employeeById);
        System.out.println(employeeById.toString());
    }

    @Test
    public void testInsertEmployee() throws Exception {
        Assert.assertNotNull(employeeService);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse("1987-12-13"); // mysql datetime format
        Employee employee = new Employee();
        employee.setTitle("Mr.");
        employee.setFirstName("ณเดช");
        employee.setLastName("คุกกี้มีไม้");
        employee.setGender("Male");
        employee.setBirthDay(date);
        employee.setFlag(null);
        employee.setHomeAddressNo("40");
        employee.setHomeRoad("เจริญกรุง");
        employee.setHomeProvince("กรุงเทพ");
        employee.setHomeDistrinct("สาทร");
        employee.setHomePostcode(10120);
        employee.setWorkAddressNo("40");
        employee.setWorkRoad("เจริญกรุง");
        employee.setWorkProvince("กรุงเทพ");
        employee.setWorkDistrinct("สาทร");
        employee.setWorkPostcode(10120);
        int effectRow = employeeService.insertEmployee(employee);
//        Assert.assertNotEquals(0,effectRow);
        System.out.println(employee.getId());
    }

    @Test
    public void testInsertCommunicationValue() throws Exception {
        Assert.assertNotNull(employeeService);
        int effectRow1 = employeeService.insertCommunicationValue(6L,2L, "test@gmail.com");
//        Assert.assertNotEquals(0,effectRow1);
        int effectRow2 = employeeService.insertCommunicationValue(6L,1L,"0899991129");
//        Assert.assertNotEquals(0,effectRow2);
    }

    @Test
    public void testUpdateEmployee() throws Exception {
        Assert.assertNotNull(employeeService);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = format.parse("1997-12-11"); // mysql datetime format
        Employee employee = new Employee();
        employee.setId(5L);
        employee.setTitle("Mr.");
        employee.setFirstName("หมากปริญ");
        employee.setLastName("เหอะๆ");
        employee.setGender("Male");
        employee.setBirthDay(date);
        employee.setFlag(null);
        employee.setHomeAddressNo("40");
        employee.setHomeRoad("เจริญกรุง");
        employee.setHomeProvince("กรุงเทพ");
        employee.setHomeDistrinct("สาทร");
        employee.setHomePostcode(10120);
        employee.setWorkAddressNo("40");
        employee.setWorkRoad("เจริญกรุง");
        employee.setWorkProvince("กรุงเทพ");
        employee.setWorkDistrinct("สาทร");
        employee.setWorkPostcode(10120);
        int effectRow = employeeService.updateEmployee(employee);
//        Assert.assertNotEquals(0,effectRow);
        System.out.println(employee.getId());
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        Assert.assertNotNull(employeeService);
        int effectRow = employeeService.deleteEmployee(3L);
//        Assert.assertNotEquals(0,effectRow);
    }

    @Test
    public void testDeleteCommunicationValue() throws Exception {
        Assert.assertNotNull(employeeService);
        int effectRow = employeeService.deleteCommunicationValue(6L);
//        Assert.assertNotEquals(0,effectRow);
        this.testInsertCommunicationValue();
    }

    @Test
    public void testFindEmployeeByValue() throws Exception {
        Assert.assertNotNull(employeeService);
        List<Employee> employees = employeeService.findEmployeeByValue("ke");
        Assert.assertNotNull(employees);
        for (Employee employee:employees){
            System.out.println(employee.toString());
        }
    }
}