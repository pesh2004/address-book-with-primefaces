package com.skoodeskill.services;

import com.skoodeskill.model.Communication;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * Created by PeshZ on 9/28/15.
 */
public class CommunicationServiceTest {
    private CommunicationService communicationService;

    @Before
    public void setUp() throws Exception {
        communicationService = new CommunicationService();
    }

    @After
    public void tearDown() throws Exception {
        communicationService = null;
    }

    @Test
    public void testGetAllCommunication() throws Exception {
        Assert.assertNotNull(communicationService);
        List<Communication> communications = communicationService.getAllCommunication();
        Assert.assertNotNull(communications);
        for (Communication communication:communications){
            System.out.println(communication.toString());
        }
    }

    @Test
    public void testGetCommunicationById() throws Exception {
        Assert.assertNotNull(communicationService);
        Communication communication = new Communication();
        communication.setId(1L);
        Communication communicationById = communicationService.getCommunicationById(communication.getId());
        Assert.assertNotNull(communicationById);
        System.out.println(communicationById.toString());
    }

    @Test
    public void testGetCommunicationByIdList() throws Exception {
        Assert.assertNotNull(communicationService);
        Communication communication = new Communication();
        communication.setId(6L);
        List<Communication> communications = communicationService.getCommunicationByIdList(communication.getId());
        Assert.assertNotNull(communications);
        System.out.println(communications.toString());
    }

    @Test
    public void testInsertCommunication() throws Exception {
        Assert.assertNotNull(communicationService);
        Communication communication= new Communication();
        communication.setName("Telephone (Test)");
        int effectRow = communicationService.insertCommunication(communication);
//        Assert.assertNoEq(0,effectRow);
        System.out.println(communication.getId());
    }

    @Test
    public void testUpdateCommunication() throws Exception {
        Assert.assertNotNull(communicationService);
        Communication communication= new Communication();
        communication.setId(4L);
        communication.setName("Telephone (Work)");
        int effectRow = communicationService.updateCommunication(communication);
//        Assert.assertNotEquals(0,effectRow);
        System.out.println(communication.getId());
    }

    @Test
    public void testDeleteCommunication() throws Exception {
        Assert.assertNotNull(communicationService);
        int effectRow = communicationService.deleteCommunication(2L);
        Assert.assertNotSame(0,effectRow);
        System.out.println(effectRow);
    }

    @Test
    public void testAddNewCommunicationRow() throws Exception {
        Assert.assertNotNull(communicationService);
        communicationService.addNewCommunicationRow("tele","0811234567");
        Assert.assertEquals(1,communicationService.getCommunicationList().size());
    }

    @Test
    public void testGetCommunicationListById() throws Exception {
        Assert.assertNotNull(communicationService);
        communicationService.addNewCommunicationRow("tele","0811234567");
        communicationService.addNewCommunicationRow("tele","dorankseo@live.com");
        System.out.println(communicationService.getCommunicationListById(1));
    }
}